
variable "region" {
  description = "Region to host this environment in"
  type        = string
  default     = "eu-west-1"
}

variable "vpc_id" {
  description = "id of the vpc to use"
  type = string
  default = "vpc-f5bd518c"
}

variable "ami_id" {
  description = "default ami for ec2 machines"
  type = string
  default = "ami-09203ef5be832b18e"
}
