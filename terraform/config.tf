#SUBNET
resource "aws_subnet" "public_demo" {
  vpc_id                  = var.vpc_id
  cidr_block              = "172.31.48.0/24"
  availability_zone       = "${var.region}a"
  map_public_ip_on_launch = true

  tags = {
    "Name" = "Demo /24 subnet"
  }
}

#EC2 INSTANCE
resource "aws_instance" "demo" {
  ami                     = var.ami_id
  availability_zone       = "${var.region}a"
  ebs_optimized           = true
  instance_type           = "t3.micro"
  monitoring              = true
  key_name                = "demo"
  disable_api_termination = false

  root_block_device {
    volume_type           = "gp2"
    volume_size           = 20
    delete_on_termination = false
  }

  network_interface {
    network_interface_id = aws_network_interface.demo.id
    device_index         = 0
  }
}

#NETWORK INTERFACE
resource "aws_network_interface" "demo" {
  security_groups   = [
    aws_security_group.allow_outbound_traffic.id,
    aws_security_group.allow_ssh_from_home.id,
    aws_security_group.allow_Gitlab_CI.id,
    aws_security_group.allow_public_inbound_web_traffic.id]
  subnet_id         = aws_subnet.public_demo.id
  private_ips       = ["172.31.48.48"]
  source_dest_check = true
  description       = "Primary network interface"
}

#ELASTIC IP
resource "aws_eip" "demo" {
  vpc      = true
  instance = aws_instance.demo.id
}

#SECURITY GROUPS
resource "aws_security_group" "allow_outbound_traffic" {
  name                   = "Allow outbound traffic"
  description            = "Allow outbound traffic"
  vpc_id                 = var.vpc_id

  egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    "Name" = "OUT - Allow all outbound traffic"
  }
}

resource "aws_security_group" "allow_public_inbound_web_traffic" {
  name                   = "Allow public inbound web traffic"
  description            = "Allow public inbound web traffic"
  vpc_id                 = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "PUBLIC - Allow public inbound web traffic"
  }
}

resource "aws_security_group" "allow_ssh_from_home" {
  name                   = "Allow ssh from home"
  description            = "Allow ssh from home"
  vpc_id                 = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["82.161.233.82/32"]
  }

  tags = {
    "Name" = "ADMIN - Allow ssh from home"
  }
}

resource "aws_security_group" "allow_Gitlab_CI" {
  name                   = "Allow Gitlab CI"
  description            = "Allow Gitlab CI"
  vpc_id                 = var.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    #cidr_blocks = ["34.74.90.64/28","34.74.226.0/24"]
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "Name" = "DEPLOY - Allow Gitlab CI"
  }
}

resource "aws_key_pair" "demo" {
  key_name   = "demo"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDi9NXIhBbI6s/bVGhKuJT13sx/V95jwZ9BuyNLueUhn862tOeri8fZGt7l3aPgZl4HTK/7uUorVWl0OasZPaLNBDUKdI7AMB7Qsj8l4L4toKkzcyYruAi5lovEph6i/DRGRQfcOs8zuT5PkQogDZhnZa8ee7qO3QB5vciddYsNYUJf1hNmZs5iRUxqPInc9d0pxwJN9dduJjkds4SnTgEcnHI6s7Aq3+sU4hbLX2ydoI+xtAt5M+Jk4tDubAWt9sN5inF4AamfdT1qyjKNW9FaM4x5HH7ew4Rl7tqtMfZ4pD0veb/9zLUvaAaMXLCjKsiayRpqPTU6VdU9YEoYUH+i/OPCncVCWhFZdNei5WCJ4fZbwsCw4JD1HpedebpE73Lpp8jeKzES16t83nSQgBf1vC681Yf3rfNjR5/RMPsKpZ8SHvvG/2YKz71X5OX+yklaF8RUqFxmqpAKb+IsUMolgF9zZRfaU1zKqpEe8L0z4Zp0w/NY487ImBzcA8FZpoPa1u8gNzK7xHXwDIutJ/udqRiwkXCHLtigVZbuy6zULyyO/MtSykK6T6G75p/1yeC1cMS6BbepCZYh1eD0dmgro5k8y1SduBWIjy/m10ZFp6Ie9ua1Zc6KOhkUhiAiMIuUcsobewYmS+6blH6ua9Q3kjDc5NHjQ0m1E6zQcXfCNQ=="
}
