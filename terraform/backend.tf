terraform {
  backend "s3" {
    bucket = "terraform.alexanderkramer.nl"
    key = "tfstate/terraform.tfstate"
    region = "eu-west-1"
  }
}
