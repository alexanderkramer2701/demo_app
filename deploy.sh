curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg && echo "docker gpg key sucesfully installed"

echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null && echo "docker repository succesfully added"

sudo apt update

sudo apt install -y docker-ce docker-ce-cli containerd.io

sudo docker run -it --rm -d -p 80:80 --name web nginx
